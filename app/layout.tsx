import type { Metadata } from "next";
import { Inter } from "next/font/google";
import "./globals.css";
import {Providers} from "./providers";
import React from "react";
import {Navbar, NavbarBrand, NavbarContent, NavbarItem, Link, Button} from "@nextui-org/react";

const inter = Inter({ subsets: ["latin"] });

export const metadata: Metadata = {
  title: "Clima",
  description: "Consulta el clima de tu ciudad",
};

export default function RootLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  return (
    <html lang="en" className='ligth'>
      <body className={inter.className}>
      <Providers>
      <Navbar className="bg-blue-500">
        <NavbarContent className="hidden sm:flex " >
          <NavbarItem className="mx-auto">
            Consulte el clima de su ciudad
          </NavbarItem>
        </NavbarContent>
      </Navbar>
      {children}
      </Providers>
      </body>
    </html>
  );
}
