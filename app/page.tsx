"use client";

import React,{useState} from 'react'
import { url } from "inspector";
import { error } from "console";
import Search from "./components/search"
import Card from "./components/card"

export default function Page() {


  let urlWeather = "https://api.openweathermap.org/data/2.5/weather?appid=4078d54ae89a452ddd32a54f1ecbfacc&lang=es";
  let cityUrl = "&q=";

  const [weather,setWeather] = useState([]);
  const[loading,setLoading] = useState(false);
  const[show,setShow] = useState(false);
  const[location,setLocation] = useState("");

  const getLocation = async(loc: React.SetStateAction<string>) => {
    setLoading(true);
    setLocation(loc);

    urlWeather = urlWeather + cityUrl + loc;

    await fetch(urlWeather).then((response)=>{
      if(!response.ok) throw {response}
      return response.json();
    }).then((weatherData) =>{
        console.log(weatherData)
        setWeather(weatherData);
        setLoading(false);
        setShow(true);
    }).catch(error => {
        console.log(error);
        setLoading(false);
        setShow(false);
    }

    )
  }

  return (
    <main className=" min-h-screen flex p-6 " >
      <div className="container">
        <Search 
          newLocation = {getLocation}
        />
        <Card 
          showData = {show}
          loadingData = {loading}
          weather={weather}
        />
      </div>      
    </main>
  )
}
