import {Input} from "@nextui-org/react";
import {Button} from '@nextui-org/button'; 
import {IconSearch} from '@tabler/icons-react'
import {useState} from 'react'

interface SearchProps {
  newLocation: any;
}

const Search : React.FC<SearchProps> = ({ newLocation }) => {
    const [city, setCity] = useState("");

    const onSubmit = (e: { preventDefault: () => void; }) => {
      e.preventDefault();
      console.log({city});
      
      if(city === "" || !city) return;

      newLocation(city);
    }
  return (

        <form onSubmit={onSubmit}>
            <div className="flex w-1/2 flex-wrap md:flex-nowrap gap-4 mb-3">
            <Input type="input" label="Ingrese la ciudad" onChange={(e)=>setCity(e.target.value)} ></Input>  
            <Button color="primary" size="lg" type="submit"><IconSearch/></Button>
            </div>
        </form>

  )
}

export default Search;