
import {Card, CardHeader, CardBody, CardFooter, Image} from "@nextui-org/react";
import {CircularProgress} from "@nextui-org/react";
import {useState} from 'react'
import 'animate.css';

interface CardInformationProps {
    showData: any;
    loadingData: boolean;
    weather: any;
}

const CardInformation: React.FC<CardInformationProps>  = ({ showData,loadingData,weather }) => {
    var url = ""
    var iconUrl = ""

    if(loadingData){
        console.log("hola");
        console.log(showData);
        <CircularProgress label="Loading..." />
    }

    if(showData){
        url = "https://openweathermap.org/img/w/"
        iconUrl = url + weather.weather[0].icon + ".png"
    }

  return (

    <div className="max-w-[900px] gap-2 px-8 ">
        {
            showData === true ? (
                <Card className="col-span-15 sm:col-span-4 h-[300px]  w-1/2 animate__animated animate__backInDown">
                    <CardHeader className="absolute z-10 top-1 flex-col !items-start">
                        <p className="text-tiny text-white/60 uppercase font-bold">{weather.name}</p>
                        <h4 className="text-white font-medium text-large">{(weather.main.temp - 273.15).toFixed(1)} °C</h4>
                        <p className="text-white font-medium text-large">
                            <Image
                            src={iconUrl}
                            alt="icon"
                            />  
                            {weather.weather[0].description} 
                        </p>
                    </CardHeader>
                    <Image
                        removeWrapper
                        alt="Card background"
                        className="z-0 w-full h-full object-cover"
                        src="https://images.pexels.com/photos/5707634/pexels-photo-5707634.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1"
                    />
                </Card>
            ):(
                <Card className="bg-white col-span-15 sm:col-span-4 w-1/2 animate__animated animate__wobble">
                <CardBody>   
                    <h2 className="text-black font-medium text-large">Sin Datos</h2>
                </CardBody>
                </Card>
            )
        }
        
    </div>
  )
}

export default CardInformation;